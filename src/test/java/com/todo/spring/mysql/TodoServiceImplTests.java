package com.todo.spring.mysql;

import com.todo.spring.mysql.models.Todo;
import com.todo.spring.mysql.repositories.SqlRepository;
import com.todo.spring.mysql.service.TodoServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TodoServiceImplTests {

    @Mock
    SqlRepository mockRepository;

    @InjectMocks
    TodoServiceImpl service;

    List<Todo> defaultTestInput = Arrays.asList(
            new Todo(1,"Title1", "Description 1", true),
            new Todo(2,"Title2", "Description 2", false),
            new Todo(3,"Title3", "Description 3", false));
    private Todo result;


    @Test
    public void getByStatus_Should_ReturnNumberOfActiveTodos_When_MatchExist_WithMockito() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(defaultTestInput);

        // Act
        List<Todo> result = service.getByStatus(true);

        // Assert
        Todo todo = result.get(0);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getTodoById_Should_ReturnTodoWithSameID_When_MatchExist_WithMockito() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(defaultTestInput);

        // Act
        List<Todo> result = Collections.singletonList(service.getTodoById(1));
        int size = result.size();


        // Assert
        Assert.assertEquals(1, size);
        Assert.assertEquals("Title1", result.get(0).getTitle());
    }
}